

//templet literals

let name = "sengu";
let designation ="softwareDevloper";
//use backticks
let log=`myself ${name}i am  a ${designation}`;



//truthy and falsy values

//there are five falsy valuse
//they are 0,"",undefined,NaN,null


//equlatity operator

let num1 = 43;
let string = "43";
//lose equlatity operator 

if(num1 == string){
    console.log("lose equals");
//this cause the type corison 
//which means that js convert string into number and compare the  value

}

//we can use Number

let number = Number("10");

if(10 === number){
    console.log("strict equals");
    //this can check the data type
}

//Experessions and statement
if(10>15){
    let sting = "go and paly";
}
// this can be complete satement

//statement in js can not exceute itself or can not produce value by itself

  
  //this can be "go and play" is Expersion
  // example for expersion
  3+4;
  true;
  false;




// terany operator

var age = 22;

age>18?console.log("work hard"):console.log("find the passion");


//functions

function calNumber() {
    return 10;

}
calNumber();
var calnum = calNumber();//return value in place of function calls
console.log(calnum);

//function declaration 
//function declaration can call before and after the function

function method(){
   return 8+10;

}

let sum = method();
console.log(sum);

//function Expression 
// function Expression can call only after the expression mentioned
// also called ananoymous function

var sum1= function (){
    return 11+1;
}
let val = sum1();
console.log(val);

//arrow function when you pass the single parameter
// this also a function Expression

var arrow = (inputs) =>"return value is here no need to write return "+inputs;

var arr = arrow("word");
console.log(arr);

var arrow2 = positive=>"make the way yourself"+positive;
var arrr = arrow2("loook");
console.log(arrr);


// arrow function with multiple parameter 
// use paranthesis when multiple parameter is there
//when you use mutiple parameter you should mentioned the return statement

var arrowReturn = (string1,string2)=>{
    var templateLiterals = `my self ${string1} and am i ${string2}`;
    return templateLiterals;
}
var temlate = arrowReturn("sengu","softwareDevloper");
console.log(temlate);

//function calling another function 

function calling(val1,val2){
     var local = arrowReturn(val1,val2);
     console.log(local);
} 
calling("kathir","full stack");