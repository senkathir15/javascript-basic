//update the key Daynamically in object using Square barcket notation

const name = {
    FristName:"senkathir"
}

console.log(name.FristName);
name.age=22;
console.log(name);

// from this also works fine 

//but 
// while using - notation we can not use . notation

const profile ={
    "frist-name":"kathir"
}
console.log(profile["frist-name"]);

/// creating daynamically key in object

let appKey = "position";
const resume ={
    [appKey]:"devloper"
}
//[appKey]:"devloper" takes the value of the variable 

console.log(resume);

// change key

const useSate = {
    name:'',
    position:'',
    age:''
}

const setSate = (key,value)=>{
    useSate[key]= value;
}

setSate("fatherName",'mathi');
setSate("name",'senkathir');
console.log(useSate);
