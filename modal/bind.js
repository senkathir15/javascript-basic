// bind is used for bind the object to function
//using reffering this keyword






//lets create an object
let profile = {
    name:"sengu",
    work:"devloper"
};
let qualification ={
    name:"stateBord",
    degree:"mechanical Engineering"
};

function printProfile(){

    //we cannot use
    console.log("hdgfjhsdgfgd")
    console.log(this.name); 
}
//reffering different object using this keywork without pointing specific name of object
printProfile();

// what happens when we use bind method 

let print = printProfile.bind(profile);
let prints = printProfile.bind(qualification);
print();
prints();
// 

// to print some hard coded value in it id

function printProfile(){

    //we cannot use
    console.log("hdgfjhsdgfgd")
    console.log(profile.name); 
    console.log(qualification.name);
}